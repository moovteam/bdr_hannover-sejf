﻿Shader "Unlit/MultipleTexture"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_SecondTex ("SD Texture", 2D) = "white" {}
		_LerpValue("Transition Float", Range(0,1)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _SecondTex;
			float4 _SecondTex_ST;
			float _LerpValue;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 addCol = tex2D(_SecondTex, i.uv);

				float depth = SAMPLE_DEPTH_TEXTURE(_SecondTex, i.uv);
				float depth2 = SAMPLE_DEPTH_TEXTURE(_MainTex, i.uv);

				float4 tempCol = col;

				if (depth > depth2) {
					tempCol = addCol;
				}
				// sample the texture
				//fixed4 col = lerp(tex2D(_MainTex, i.uv), tex2D(_SecondTex, i.uv),_LerpValue);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return tempCol;
			}
			ENDCG
		}
	}
}
