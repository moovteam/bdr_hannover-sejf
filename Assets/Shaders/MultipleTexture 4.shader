﻿Shader "Unlit/AdditiveDoubleSidedBlackness"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	_ColorAmplify("Color Amp", Range(1,20)) = 1
		_AlphaDistanceTreshold("Distance", Range(0.001, 100)) = 0.5
		_Alpha("Alpha", Range(0,1)) = 1
	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		ZWrite Off
		Blend SrcAlpha DstAlpha
		Cull Off

		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 worldPos : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MulColor;
			float _AlphaDistanceTreshold;
			float _ColorAmplify;
			float _Alpha;
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{

			fixed4 col = tex2D(_MainTex, i.uv);

			float blackness = (col.r + col.g + col.b) / 3.0;
			float4 tempCol = col;

			float dist = distance(i.worldPos, _WorldSpaceCameraPos);
			tempCol *= _ColorAmplify;
			
			tempCol.a =blackness


				UNITY_APPLY_FOG(i.fogCoord, col);
				return tempCol;
			}
			ENDCG
		}
	}
}
