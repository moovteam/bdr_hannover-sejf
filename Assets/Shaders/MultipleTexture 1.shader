﻿Shader "Unlit/Additive"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_MulColor("Multiply color", Color) = (1, 0, 0, 1) // color
	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		AlphaToMask Off
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite On
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MulColor;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{

			fixed4 col = tex2D(_MainTex, i.uv);

			float blackness = (col.r + col.g + col.b) / 3.0;
			float4 tempCol = col;


			tempCol *=2;
			tempCol.a = blackness;

				UNITY_APPLY_FOG(i.fogCoord, col);
				return tempCol;
			}
			ENDCG
		}
	}
}
