﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineInherit : MonoBehaviour {

    bool s_AnimInPogress = false;
    float distance;
    float counter = 0;
    [SerializeField]
    float _speed = 100f;

    Vector3 origin;
    Vector3 destination;

    public void DrawLine(Vector3 startPosition, Vector3 endPosition, float speed)
    {
        distance = Vector3.Distance(startPosition, endPosition);
        origin = startPosition;
        destination = endPosition;
        _speed = speed;
        s_AnimInPogress = true;
    }
	
	// Update is called once per frame
	void Update () {

        if (s_AnimInPogress)
        {
            if(counter < distance)
            {
                counter += .5f / _speed;

                float x = Mathf.Lerp(0, distance, counter);

                Vector3 PointAlongLine = x * Vector3.Normalize(destination - origin) + origin;

                gameObject.GetComponent<LineRenderer>().SetPosition(1, PointAlongLine);

            }
            else
            {
                s_AnimInPogress = false;
            }
        }
	}
}
