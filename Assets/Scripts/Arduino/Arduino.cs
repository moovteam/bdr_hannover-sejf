﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System;

public class Arduino : MonoBehaviour {
    
    SerialPort stream = new SerialPort("COM4", 115200);
    public int value;
    bool isRunning = true;

    [SerializeField]
    int variable;

    private void OnDisable()
    {
        isRunning = false;
    }

    // Use this for initialization
    void Start () {
        try
        {
            stream.ReadTimeout = 2;
            stream.DtrEnable = true;
          
            stream.DtrEnable = true;
            stream.DataReceived += GetDataFromArduino;
            stream.ErrorReceived += ErrorFromArduino;
            stream.DtrEnable = true;
            stream.Open();
            Debug.Log("Polaczenie otworzone");
        }
        catch(Exception e)
        {
            Debug.Log("Problem inicjalizacji: " + e);
        }

        StartCoroutine(GetDataArduino());

    }
	
    

    void GetDataFromArduino(object sender, SerialDataReceivedEventArgs e)
    {
    }

    void ErrorFromArduino(object sender, SerialErrorReceivedEventArgs e)
    {
        Debug.Log("Error: " + e);
    }

    public IEnumerator GetDataArduino()
    {

        while (isRunning)
        {
            try
            {
                try
                {
                    value = int.Parse(stream.ReadLine());
                }catch(Exception e)
                {
                    
                }
                
                GoToState(Mathf.Abs(value));
                //ActivateLaser(Mathf.Abs(value));
                SetState(Math.Abs(value));
            }
            catch (TimeoutException)
            {

            }
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForFixedUpdate();
    }

    void GoToState(int value)
    {
        if(value < 20)
        {
            StateManager.Instance.StateBegin();
        }else if(value >= 20 && value < 40)
        {
            StateManager.Instance.StateMiddle();
        }else if(value >= 40)
        {
            StateManager.Instance.StateEnd();
        }
    }

    /*
    void ActivateLaser(int value)
    {
        if(value < 8)
        {
            try
            {

                for (int i = 0; i < 14; i++)
                {
                    Laser.Instance.StageOneLasers[i].gameObject.SetActive(false);
                }

                for (int i = 0; i <= value; i++)
                {
                    variable = i;
                    Laser.Instance.StageOneLasers[i].gameObject.SetActive(true);
                    Laser.Instance.StageOneLasers[i * 2].gameObject.SetActive(true);
                }

            }
            catch(Exception e)
            {

            }

        }

        if(value == 8)
        {
            for (int i = 0; i < 14; i++)
            {
                Laser.Instance.StageOneLasers[i].gameObject.SetActive(true);
            }
        }
    }*/

    void SetState(int indexState)
    {
        switch (indexState)
        {
            case 0:
                {
                    for(int i = 0; i < Laser.Instance.LaserOne.Length; i++)
                    {
                     
                        StartCoroutine(ActivateLaser(i, .2f, 1, 6));
                    }


                    StartCoroutine(DeactivateLaser(0, .2f, 1, 4));

                    break;
                }


            case 3:
                {
                    StartCoroutine(DeactivateLaser(0, .2f, 2, 2));
                    StartCoroutine(ActivateLaser(0, .2f, 2, 4));
                    
                    break;
                }

            case 5:
                {
                    StartCoroutine(ActivateLaser(0, .2f, 3, 2));
                    break;
                }
        }
    }

    IEnumerator ActivateLaser(int index, float time, int stage, int length)
    {
        yield return new WaitForSeconds(time);

        switch (stage)
        {
            case 1:
                {
                 
                    Laser.Instance.LaserOne[index].gameObject.SetActive(true);

                    break;
                }

            case 2:
                {
                    Laser.Instance.LaserTwo[index].gameObject.SetActive(true);

                    if(index < length)
                    {
                        StartCoroutine(ActivateLaser(index + 1, .2f, 2, 4));
                    }

                    break;
                }

            case 3:
                {
                    Debug.Log("jestem tutaj");
                    Laser.Instance.LaserThree[index].gameObject.SetActive(true);

                    if (index < length)
                    {
                        StartCoroutine(ActivateLaser(index + 1, .2f, 3, 2));
                    }

                    break;
                }
        }
        
    }

    IEnumerator DeactivateLaser(int index, float time, int stage, int length)
    {
        yield return new WaitForSeconds(time);

        switch (stage)
        {

            case 1:
                {
                        Laser.Instance.LaserTwo[index].gameObject.SetActive(false);
                        if (index < length-2)
                        {
                            StartCoroutine(DeactivateLaser(index + 1, .2f, 1, 6));
                        }
                    
                    break;
                }

            case 2:
                {
                    Laser.Instance.LaserThree[index].gameObject.SetActive(false);
                    if(index < length-1)
                    {
                        StartCoroutine(DeactivateLaser(index + 1, .2f, 2, 3));
                    }
                    break;
                }
        }
    }
}
