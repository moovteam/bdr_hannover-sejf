﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Singleton<Laser> {

    bool isOrigin = true;

    public GameObject OriginLaserPrefab;
    public GameObject LaserPrefab;
    public Transform ParentLasers;

    private GameObject _laser;

    public List<GameObject> l_lasers;

    public GameObject[] StageOneLasers = new GameObject[6];


    public GameObject[] LaserOne = new GameObject[0];
    public GameObject[] LaserTwo = new GameObject[0];
    public GameObject[] LaserThree = new GameObject[0];

	public void CreateLaser()
    {
        if (isOrigin)
        {
            isOrigin = false;
            _laser = Instantiate(OriginLaserPrefab, ParentLasers);
            l_lasers.Add(_laser);
            SetRotation(_laser);
            return;
        }

        _laser = Instantiate(LaserPrefab, ParentLasers);
        l_lasers.Add(_laser);
        SetRotation(_laser);
    }

    void SetRotation(GameObject laser)
    {
        laser.transform.localRotation = Quaternion.Euler((new Vector3(Random.Range(0, 360), -90, 90)));
        laser.transform.localPosition = new Vector3(0, Random.Range(-4, -14), 8);
    }

    public void DestroyLaser(int id)
    {
        Destroy(l_lasers[id]);
        l_lasers.Remove(l_lasers[id]);
    }

}
