﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawAnim : MonoBehaviour {

    public Transform LinesParent;

    public GameObject LinePrefab;
    private GameObject _line;
    private LineRenderer s_lineRenderer;

    private float dist;
    private float counter;

    private Vector3 origin;
    private Vector3 destination;

    public float LineDrawDuration = 500f;

    [SerializeField]
    bool drawLine = false;
	
    public void DrawLine()
    {
        _line = Instantiate(LinePrefab, LinesParent);
        s_lineRenderer = _line.GetComponent<LineRenderer>();
        s_lineRenderer.SetWidth(5f, 5f);

        Vector3 originPosition = new Vector3(Random.RandomRange(0, Screen.width), 0, Random.Range(600, 1500));

        s_lineRenderer.SetPosition(0, originPosition);

        Vector3 destinationPosition = new Vector3(Random.RandomRange(0, Screen.width), Screen.height, Random.Range(600, 1500));

        _line.GetComponent<LineInherit>().DrawLine(originPosition, destinationPosition, LineDrawDuration);
    }

	// Update is called once per frame
	void Update () {
        if (drawLine)
        {
            drawLine = false;
            DrawLine();
        }
	}
}
